func create(outer_r : float , inner_r : float , height : float , material : SpatialMaterial) -> CSGCombiner:
	var foundation = CSGCombiner.new()
	var bowl_inner = CSGSphere.new()
	var top = CSGBox.new()
	var land = CSGBox.new()

	var dr = outer_r - inner_r
	var dh = height - dr

	foundation.use_collision = 1
	
	bowl_inner.radius = inner_r
	bowl_inner.material = material
	bowl_inner.operation = CSGShape.OPERATION_INTERSECTION
	bowl_inner.smooth_faces = 1
	bowl_inner.radial_segments = 20
	bowl_inner.rings = 20
#	bowl_inner.invert_faces = 1
	
	top.depth = 2*outer_r
	top.height = 2*outer_r
	top.width = 2*outer_r
	top.material = material
	top.operation = CSGShape.OPERATION_SUBTRACTION
	top.translation = Vector3(0 , height , 0)
#	top.invert_faces = 1

	land.depth = 2*inner_r
	land.height = dh
	land.width = 2*inner_r
	land.material = material
	land.operation = CSGShape.OPERATION_INTERSECTION
	land.translation = Vector3(outer_r , dh/2-inner_r , 0)

	foundation.add_child(bowl_inner)
#	foundation.add_child(top)
	foundation.add_child(land)
	foundation.translation = Vector3(0 , outer_r , 0)
	return foundation
