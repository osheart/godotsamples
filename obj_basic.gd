func sphere(radius : float, position : Vector3, material : SpatialMaterial, collision : bool = 1) -> CSGCombiner:
	var obj = CSGCombiner.new()
	var shape = CSGSphere.new()
	shape.radius = radius
	shape.translate(position)
	shape.material = material
	
	obj.use_collision = collision
	obj.add_child(shape)
	return obj

func polygon(corners : PoolVector2Array, height : float, material : SpatialMaterial, rotate : float = 90, collision : bool = true) -> CSGCombiner:
	#	TODO: Rotation to fit with normal input
	var obj = CSGCombiner.new()
	var shape = CSGPolygon.new()
	
	obj.use_collision = collision
	shape.material = material
	shape.depth = height
	shape.polygon = corners
	shape.rotate_x(deg2rad(rotate))
	shape.translate(Vector3(0,0,height))
	obj.add_child(shape)
	return obj

func cylinder(radius : float, height : float, position : Vector3, material : SpatialMaterial, sides : int = 8, collision : bool = true) -> CSGCombiner:
	#	TODO: Rotation to fit with normal input
	var obj = CSGCombiner.new()
	var shape = cylinder_shape(radius, height, position, material, sides)
	
	obj.use_collision = collision
	obj.add_child(shape)
	return obj

func cone(radius : float, height : float, position : Vector3, material : SpatialMaterial, sides : int = 8, collision : bool = true) -> CSGCombiner:
	#	TODO: Rotation to fit with normal input
	var obj = CSGCombiner.new()
	var shape = cylinder_shape(radius, height, position, material, sides)
	shape.cone = true
	
	obj.use_collision = collision
	obj.add_child(shape)
	return obj

func cylinder_shape(radius : float, height : float, position : Vector3, material : SpatialMaterial, sides : int) -> CSGCylinder:
	var shape = CSGCylinder.new()
	shape.material = material
	shape.sides = sides
	shape.depth = height
	shape.radius = radius
	shape.translate(position)
	return shape

func torus(inner_r : float, outer_r : float, position : Vector3, material : SpatialMaterial, normal : Vector3 = Vector3(1,0,0), sides : int = 8, ring_sides : int = 6, collision : bool = true):
#	TODO: Rotation to fit with normal input
	var obj = CSGCombiner.new()
	var shape = CSGTorus.new()
	
	shape.inner_radius = inner_r
	shape.outer_radius = outer_r
	shape.material = material
	shape.sides = sides
	shape.ring_sides = ring_sides
	
	obj.use_collision = collision
	obj.add_child(shape)
	return obj

func box(depth : float, height : float, width : float, material : SpatialMaterial, normal : Vector3 = Vector3(1,0,0), collision : bool = true):
#	TODO: Rotation to fit with normal input
	var obj = CSGCombiner.new()
	var shape = CSGBox.new()
	
	shape.depth = depth
	shape.width = width
	shape.height = height
	shape.material = material
	
	obj.use_collision = collision
	obj.add_child(shape)
	return obj

#TODO: Spheroids
