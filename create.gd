func cube(edge_length : float, position : Vector3, material : SpatialMaterial) -> StaticBody:
	var plusminus = Vector2(-1, 1)
	var offset = edge_length/2

	var cube = StaticBody.new()
	var cube_meshinstance = MeshInstance.new()
	var cube_collision = CollisionShape.new()
	var cube_shape = ConvexPolygonShape.new()
	var cube_mesh = Mesh.new()
	var surface = SurfaceTool.new()
	surface.begin(Mesh.PRIMITIVE_TRIANGLES)
	surface.set_material(material)

	for i in plusminus:
		for j in plusminus:
			for k in plusminus:
				if i == -1:
					surface.add_vertex(Vector3(position.x + i*offset, position.y + j*offset, position.z + k*offset))
					surface.add_vertex(Vector3(position.x + i*offset, position.y - j*offset, position.z + k*offset))
					surface.add_vertex(Vector3(position.x + i*offset, position.y + j*offset, position.z - k*offset))

	surface.commit(cube_mesh)
	cube_meshinstance.mesh = cube_mesh
	cube.add_child(cube_meshinstance)
	#cube_shape.set_faces(cube_mesh.get_faces())
	#cube_shape.points =
	cube_collision.set_shape(cube_shape)
	cube.add_child(cube_collision)
	return cube

func bowl(radius_max : float, height_max : float, thickness : float, origo : Vector3, material : SpatialMaterial) -> StaticBody:
	var n_radial = 12 # radial resolution
	var n_angular = 12 # angular resolution         
	var radius_min = 0
	var radius_inside = sqrt(radius_max * radius_max - thickness / (height_max/(radius_max*radius_max)))

	var bowl = StaticBody.new()
	var st = surface_init(material)
	var verticies = bowl_builder(radius_min, radius_inside, height_max - thickness, origo + Vector3(0, thickness, 0), n_radial, n_angular)
	verticies = append_backwards(verticies, bowl_builder(radius_min, radius_max, height_max, origo, n_radial, n_angular))
	verticies.append_array(bowl_edge(radius_max, radius_inside, height_max, origo, n_angular))
	st = surface_build(st, verticies)
	bowl = surface_exec(bowl, st)
	return bowl

func bowl_point(radius, height, angle, origo) -> Vector3:
	# calculate a point in the parabeloid
	var point = origo + Vector3(radius * cos(angle), height, radius * sin(angle))
	return point

func bowl_incline(radius_min, radius_max, parabola_a, n_radial) -> float:
	# Intervals are distributed by incline rather than distance, to optimize object smoothness.
	var incline_min = 2 * parabola_a * radius_min
	var incline_max = 2 * parabola_a * radius_max
	var incline_step = (incline_max - incline_min)/n_radial
	return incline_step

#func bowl_build():
#
#func bowl_crosssection(sphere_r : float = 4, cross_r : float = 4, thickness : float = 0.1, n : int = 10):
#	var verticies = PoolVector3Array()
#	var angle = asin(cross_r / sphere_r)
#	var angle_step = angle / (n-1)
#	var loop = range(n)
#
#	for i in loop:
#		verticies.push_back(circle_vertex(sphere_r , angle_step * i))
#
#	loop.invert()
#	for i in loop:
#		verticies.push_back(Vector3(0 , thickness , 0) + circle_vertex(sphere_r , angle_step * i))
#
#func circle_vertex(r : float , a : float) -> Vector3:
#	var v = Vector3(sin(a) * r , r - cos(a) * r , 0)
#	return v
#
#func rotate_crosssection(crosssection : PoolVector3Array , surface : SurfaceTool , n : int = 10):
#	var angle_loop = range(n-2)
#	var angle_step = 2*PI / n
#	var angle = 0
#
#	for i in angle_loop:
#		for vertex in crosssection:
#			if i != 1 || vertex.x != 0:
#
#
#func rotate_vertex(crosssection_vertex : Vector3 , angle : float) -> Vector3:
#	var vertex = Vector3(crosssection_vertex.x * cos(angle) , crosssection_vertex.y , crosssection_vertex.x * sin(angle))
#	return vertex

func bowl_builder(radius_min : float, radius_max : float, height_max : float, origo : Vector3, n_radial : int, n_angular : int) -> PoolVector3Array:
	var angle_min = 0
	var angle_max = 2*PI
	var angle_left = 0
	var angle_right = 0
	var height_inner = 0
	var height_outer = 0
	var radius_inner = 0
	var radius_outer = 0
	var p_inner_left = Vector3()
	var p_inner_right = Vector3()
	var p_outer_left = Vector3()
	var p_outer_right = Vector3()
	var verticies = PoolVector3Array()
	
	var parabola_a = height_max/(radius_max*radius_max)
	var incline_step = bowl_incline(radius_min, radius_max, parabola_a, n_radial)
	var angle_step = (angle_max-angle_min)/n_angular
	
	for n in range(n_radial): 
		radius_outer = (n+1) * incline_step / (2 * parabola_a)
		radius_inner = n * incline_step / (2 * parabola_a)
		height_inner = parabola_a * radius_inner * radius_inner
		height_outer = parabola_a * radius_outer * radius_outer
		for s in range(n_angular):
			angle_left = angle_min + s * angle_step
			angle_right = angle_min + (s+1) * angle_step
			p_outer_left = bowl_point(radius_outer, height_outer, angle_left, origo)
			p_outer_right = bowl_point(radius_outer, height_outer, angle_right, origo)
			if n == 0: # first ring is unique due to single central point
				p_inner_left = origo
			else: # other rings are generated as squares and filled with 2 triangles
				p_inner_left = bowl_point(radius_inner, height_inner, angle_left, origo)
				p_inner_right = bowl_point(radius_inner, height_inner, angle_right, origo)
				verticies = append_triangle(verticies, p_inner_left, p_outer_right, p_inner_right)
			verticies = append_triangle(verticies, p_inner_left, p_outer_left, p_outer_right)
	return verticies

func bowl_edge(radius_outer : float, radius_inner : float, height, origo, n_angular) -> PoolVector3Array:
	var angle_range = range_circle(n_angular)
	var p_inner_left = Vector3()
	var p_inner_right = Vector3()
	var p_outer_left = Vector3()
	var p_outer_right = Vector3()
	var verticies = PoolVector3Array()
	
	for s in range(n_angular):
		p_outer_left = bowl_point(radius_outer, height, angle_range[s], origo)
		p_outer_right = bowl_point(radius_outer, height, angle_range[s+1], origo)
		p_inner_left = bowl_point(radius_inner, height, angle_range[s], origo)
		p_inner_right = bowl_point(radius_inner, height, angle_range[s+1], origo)
		verticies = append_triangle(verticies, p_inner_left, p_outer_left, p_outer_right)
		verticies = append_triangle(verticies, p_inner_left, p_outer_right, p_inner_right)
	return verticies

func surface_init(material : SpatialMaterial) -> SurfaceTool:
	var surface = SurfaceTool.new()
	surface.begin(Mesh.PRIMITIVE_TRIANGLES)
	surface.set_material(material)
	return surface

func surface_build(surface : SurfaceTool, verticies : PoolVector3Array) -> SurfaceTool:
	for vertex in verticies:
		surface.add_vertex(vertex)
	return surface

func surface_exec(parent, surface):
	var bowl_meshinstance = MeshInstance.new()
	var bowl_collision = CollisionShape.new()
	var bowl_shape = ConcavePolygonShape.new()
	var bowl_mesh = Mesh.new()

	surface.commit(bowl_mesh)
	bowl_meshinstance.mesh = bowl_mesh
	parent.add_child(bowl_meshinstance)
	bowl_shape.set_faces(bowl_mesh.get_faces())
	bowl_collision.set_shape(bowl_shape)
	parent.add_child(bowl_collision)
	return parent

func append_backwards(a : PoolVector3Array, b : PoolVector3Array) -> PoolVector3Array:
	var counter = range(b.size()-1,-1,-1)
	for i in counter:
		a.push_back(b[i])
	return a

func append_triangle(v : PoolVector3Array, p1 : Vector3, p2 : Vector3, p3 : Vector3) -> PoolVector3Array:
	v.push_back(p1)
	v.push_back(p2)
	v.push_back(p3)
	return v

func range_circle(n):
	var r = range(n+1)
	var step = 2*PI/n
	for i in r:
		r[i] = i * step
	return r
